package com.example.mullans.myapplication;

/**
 * Created by dangquang2011 on 07.04.15.
 */
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    // These constants are used to facilitate communication between the MainActivity
    // and the CowActivity. See the startActivityForResult call below.
    public static final int COW_REQUEST_CODE = 1;
    public static final String COW_NAME_KEY = "cow_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_num) {
            Intent intent = new Intent(this, NumberActivity.class);
            startActivity(intent);
            return true;
        }else if(id == R.id.action_star){
            Intent intent = new Intent(this, StarActivity.class);
            startActivity(intent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case MainActivity.COW_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String cowName = data.getStringExtra(MainActivity.COW_NAME_KEY);
                    String message = "The cow's name is '" + cowName + "'";
                    Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
        }
    }
}
